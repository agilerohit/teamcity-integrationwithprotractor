/**
 * Created by rohitd on 24-05-2018.
 */

var DiscussPage;
DiscussPage = function() {

    //Locators
    var DiscussLbl = element(by.xpath("//a[text()='Discuss']"));

    this.getURL = function () {
    browser.get('https://angularjs.org/');
    browser.ignoreSynchronization = true;
    };

    //Click on Discuss Link
    this.clickonDiscussLink = function () {
    DiscussLbl.click();
    };

    };

module.exports = DiscussPage;
