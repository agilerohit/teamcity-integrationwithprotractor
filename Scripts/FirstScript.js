/**
 * Created by rohitd on 08-07-2018.
 */
/**
 * Created by rohitd on 24-05-2018.
 */
    var HomePageHelper = require('../HelperFile/HomePageHelper.js');
    var readJsonFile = require('../UtilFile/PropertyFile.json');
    expectedurl = "http://www.webdriverjs.com/?s=protractor";

    describe('Home Page Testing' ,function(){
    browser.ignoreSynchronization = true;
    var homePageHelper = new HomePageHelper();

    it('Verify Home Page Element', function(){
    browser.get("http://webdriverjs.com");
    homePageHelper.verifyHomePageText();
    expect(homePageHelper.searchicon.isPresent()).toEqual(true);
    homePageHelper.clickOnGETSTARTED();
    expect(homePageHelper.linkedinlink.isPresent()).toEqual(true);
    });

    it('Select Language', function(){
    expect(homePageHelper.selectlang.$('option:checked').getText()).toEqual('Select Language');
    homePageHelper.selectLanguage();
    });

    it('Search any Keyword', function(){
    homePageHelper.enterSearchkeyword(readJsonFile.searchkeyword);
    homePageHelper.clickOnSearchIcon();
    expect(browser.getCurrentUrl('/')).toEqual(expectedurl);
    });

});

