var SpecReporter = require('jasmine-spec-reporter').SpecReporter;
var HtmlReporter = require('protractor-beautiful-reporter');


exports.config = {
    seleniumAddress: 'http://localhost:4444/wd/hub',
    directConnect: true,
    capabilities: {
        'browserName': 'chrome'
    },
    framework: 'jasmine2',

    jasmineNodeOpts :
    { showColors: true, defaultTimeoutInterval : 1000000
    },

       allScriptsTimeout: 4000,
   // specs: ['../Scripts/FirstScript.js','../TestScripts*//*.js'],

    suites:{
     SmokeTest: ['../TestScripts*//*.js'],
     E2ETest: ['../Scripts*//*.js']
    },


        onPrepare: function () {
                jasmine.getEnv().addReporter(new SpecReporter({
                displayStacktrace: true
            }))

            jasmine.getEnv().addReporter(new HtmlReporter({
                baseDirectory: '../Reports/screenshot',
                takeScreenshots: 'true',
                consolidateAll: 'false',
                showSummary: 'true',
                takeScreenshotOnlyOnFailures: 'true',
                docName: 'index.html',
                cleanDestination: true,
                preserveDirectory: 'true'
            }).getJasmine2Reporter());

            browser.driver.manage().window().maximize();

            if (process.env.TEAMCITY_VERSION) {
            require('jasmine-reporters');
            jasmine.getEnv().addReporter(new jasmine.TeamcityReporter());
            }


            var originalTimeout;

            beforeEach(function () {
                originalTimeout = jasmine.DEFAULT_TIMEOUT_INTERVAL;
                jasmine.DEFAULT_TIMEOUT_INTERVAL = 100000;
            });

            afterEach(function () {
                jasmine.DEFAULT_TIMEOUT_INTERVAL = originalTimeout;
                /* var obj = {
                 "project_name":"IMS Web",
                 "plan_name":"MyTestPlan",
                 "section_name":"Collections",
                 "title":jasmine.results.spec.fullName,
                 "status_name":(jasmine.results.spec.failedExpectations.length === 0 ? "passed" : "failed")
                 };
                 tr.ifNeededCreateThenAddResultForCase(obj).finally(function(){
                 done();
                 });
                 }); */

            });
        },

        jasmineNodeOpts: {
            showColors: true, // Use colors in the command line report.
                includeStackTrace: true,
                defaultTimeoutInterval: 1000000
        }



    };



