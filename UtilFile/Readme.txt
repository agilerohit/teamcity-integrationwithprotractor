suites: {
    smoke: [
        'login.spec.js',
        'someOthertest.spec.js'
    ],
    performance: [
        'someOthertest.spec.js',
        'performance/*.spec.js'
    ]
},
single suite protractor conf.js --suite E2ETest

multiple suites protractor conf.js --suite E2ETest,B,C


suites: {
  A: ['./Spec/Find_Account_Page/FindAccount_By_AccountInfo.js', './Spec/Find_Account_Page/FindAccount_By_UserInfo.js'],
  B: ['./Spec/LIST/*Spec.js'],
  C: ['./Spec/USER_ROLES/ACCMANG_USER_Role/*Spec.js'],
  D: ['./Spec/USER_ROLES/ADMIN_USER_Role/*Spec.js']
 },