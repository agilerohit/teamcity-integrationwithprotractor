/**
 * Created by rohitd on 25-05-2018.
 */

var UpworkLoginPage;
UpworkLoginPage = function() {

    //Locators
    var signupLink = element(by.xpath("//a[@data-qa='signup'][@class='text-uppercase']"));
    var firstnametextbox = element(by.xpath("//input[@name='firstName']"));

    this.getURL = function () {
    browser.get('https://www.upwork.com/ab/account-security/login');
    browser.ignoreSynchronization = true;
    };

    //Click on SignUp Link
    this.clickonSignUpLink = function () {
    signupLink.click();
    };

    //Input UserName
    this.EnterFirstName = function (username) {
    firstnametextbox.sendKeys(username);
    };

};

module.exports = UpworkLoginPage;
