/**
 * Created by rohitd on 24-05-2018.
 */
    var LearnPage;

    LearnPage = function() {

    //Locators
    var learnLbl = element(by.xpath("//a[text()='Learn']"));
    var tutorialLbl = element(by.xpath("//a[text()='Tutorial']"));
    var faqLbl = element(by.xpath("//a[text()='FAQ']"));
    var videoLbl = element(by.xpath("//a[text()='Videos']"));
    var caseStudiesLbl = element(by.xpath("//a[text()='Case Studies']"));

    this.getURL = function () {
    browser.get('https://angularjs.org/');
    browser.ignoreSynchronization = true;
    };

    //Click on Learn Link
    this.clickonLearnLink = function () {
    learnLbl.click();
    };

    //Click on Tutorial Link
    this.clickonTutorialLink = function () {
    tutorialLbl.click();
    };

    //Click on FAQ Link
    this.clickonFAQLink = function () {
    faqLbl.click();
    };

    //Click on Video Link
    this.clickonVideoLink = function () {
    videoLbl.click();
    };

    //Click on CaseStudies Link
    this.clickonCaseStudiesLink = function () {
    caseStudiesLbl.click();
    };
};
module.exports = LearnPage;