/**
 * Created by rohitd on 08-07-2018.
 */
var HomePageHelper;
HomePageHelper = function ()
{
//Locators
var hometxt = element(by.xpath("(//a[text()='Home'])[1]"));
var protractortxt= element(by.xpath ("//a[text()='Protractor']"));
var serachtxt = element(by.xpath("//input[@placeholder='Search …']"));
this.searchicon = element(by.xpath("//i[@class='glyphicon glyphicon-search homePageSearchIcon']"));
var progetstartedtxt = element(by.xpath("(//button[@class='btn btn-success'])[1]"));
var prodroplink = element(by.xpath("(//button[@class='btn btn-success dropdown-toggle'])[1]"));
var statictxt = element(by.xpath("(//li[text()='Recent posts'])[1]"));
this.linkedinlink = element(by.xpath("//a[@href='https://www.linkedin.com/in/vijaykumardaram']"));
this.selectlang = element(by.xpath("//select[@class='goog-te-combo']"));
this.selectlang1 = element.all(by.id(":0.targetLanguage"));


   //Methods
    this.verifyHomePageText = function() {
    hometxt.getText().then(function (text) {
    console.log(text);
    expect(text).toContain('HOME');
    });


    protractortxt.getText().then(function (text) {
    console.log(text);
    expect(text).toContain('PROTRACTOR');
    })

    progetstartedtxt.getText().then(function (text) {
    console.log(text);
    expect(text).toContain('GET STARTED');
    })

    }

    //Method to click on GET STARTED ( Protractir one)
    this.clickOnGETSTARTED = function()
    {
    prodroplink.click();
    statictxt.getText().then(function (text) {
    console.log(text);
    expect(text).toContain('Recent posts');
    })
    }

    //Method to Select Language
    this.selectLanguage = function()
    {
    this.selectlang.click();
    this.selectlang.$('[value=am]').click();
    }
    //Method to enter Search Keyword
    this.enterSearchkeyword = function (input)
    {
    serachtxt.sendKeys(input) ;
    }

    //Method to click on Search Icon
    this.clickOnSearchIcon = function()
    {
    this.searchicon.click();
    }







}


module.exports = HomePageHelper;
